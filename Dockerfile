FROM openjdk:16-jdk
ADD build/libs/java-player-template-0.1.jar java-player-template-0.1.jar

EXPOSE 8080
EXPOSE 8081

#CMD ["java", "-Denv=${env}", "-Dspring.profiles.active=${env}", "-jar", "jean-grey-core-0.1.jar"]
CMD ["java", "-jar", "java-player-template-0.1.jar"]
