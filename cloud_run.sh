#!/usr/bin/env bash
echo Deploying Cloud Run for project $1 and login $2
gcloud run deploy $2-player --image gcr.io/$1/$2-player --platform managed --region us-west1 --allow-unauthenticated