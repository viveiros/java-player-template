# Paul the Octopus Java Player

This project will help you to create a player for the Paul the Octopus competition using Java (Spring Boot) and 
GCP Cloud Run (serverless environment for containers).

## How to use this project?

### Pre-requisites

**1. JDK 16** (it may work with older versions... let us know :-))

You must be able to open a terminal, type "java -version", and get something like what you see
in the box bellow. If not, please Google how to install a JDK.

```
> java -version
java version "16.0.1" 2021-04-20
Java(TM) SE Runtime Environment (build 16.0.1+9-24)
Java HotSpot(TM) 64-Bit Server VM (build 16.0.1+9-24, mixed mode, sharing)
(base) dviveiros@iMac-2020 java-player-template % 
```

**2. Gradle 7.1** (it may work with older versions... again, let us know :-))

You must be able to open a terminal, type "gradle -version", and get something like what you see
in the box bellow. If not, please Google how to install Gradle.

```
> gradle -version

------------------------------------------------------------
Gradle 7.1
------------------------------------------------------------

Build time:   2021-06-14 14:47:26 UTC
Revision:     989ccc9952b140ee6ab88870e8a12f1b2998369e

Kotlin:       1.4.31
Groovy:       3.0.7
Ant:          Apache Ant(TM) version 1.10.9 compiled on September 27 2020
JVM:          16.0.1 (Homebrew 16.0.1+0)
OS:           Mac OS X 11.4 x86_64
```

**3. Google Cloud SDK**

You must be using the lastest version of Google Cloud SDK.

**IMPORTANT STEPS:**

- Download and install Google Cloud SDK (again, Google how to do it)
- Run the authentication process. Please use your @ciandt.com account.
- Make sure that you have access to phoenix-cit project. Let us know that you want this access.
- Make phoenix-cit your default project

Here is what you expect to see, if everything is good to go:

```
> gcloud config get-value project
phoenix-cit
```

### Configuring your environment

After cloning/forking this project, you need to do a couple of things.

**1. Configure your login** 

Open the file **gradle.properties** and change the property bellow:

```
myLogin=<mylogin> // add your login here
```

**2. Run chmod for the .sh files**

```
> chmod 755 ./cloud_build.sh
> chmod 755 ./cloud_run.sh
```

**3. Make a few changes on PredictionsController.java**

```
//PredictionsController.java

//TODO: Put your secret here
private static final String MY_SECRET = "<YOUR SECRET GOES HERE>";
//TODO: Put your player ID here
private static final Integer MY_PLAYER_ID = <YOUR PLAYER ID GOES HERE>;
```

If you don't know your secret or player id, talk to the admin group
(paul-octopus-admin@ciandt.com)

### Testing if everything is OK

**1. Running it locally**

```
On the root dir:
> gradle bootRun

Task :bootRun

  _____            _   _   _             ____       _
 |  __ \          | | | | | |           / __ \     | |
 | |__) |_ _ _   _| | | |_| |__   ___  | |  | | ___| |_ ___  _ __  _   _ ___ 
 |  ___/ _` | | | | | | __| '_ \ / _ \ | |  | |/ __| __/ _ \| '_ \| | | / __|
 | |  | (_| | |_| | | | |_| | | |  __/ | |__| | (__| || (_) | |_) | |_| \__ \
 |_|   \__,_|\__,_|_|  \__|_| |_|\___|  \____/ \___|\__\___/| .__/ \__,_|___/
                                                            | |              
                                                            |_|              
2021-06-24 09:38:51.770  INFO 35282 --- [           main] com.paultheoctopus.player.Application    : Starting Application using Java 16.0.1 on iMac-2020.local with PID 35282 (/Users/dviveiros/git/java-player-template/build/classes/java/main started by dviveiros in /Users/dviveiros/git/java-player-template)
2021-06-24 09:38:51.771 DEBUG 35282 --- [           main] com.paultheoctopus.player.Application    : Running with Spring Boot v2.5.1, Spring v5.3.8
2021-06-24 09:38:51.772  INFO 35282 --- [           main] com.paultheoctopus.player.Application    : No active profile set, falling back to default profiles: default
2021-06-24 09:38:52.552  INFO 35282 --- [           main] com.paultheoctopus.player.Application    : Started Application in 1.209 seconds (JVM running for 1.443)
<==========---> 80% EXECUTING [54s]
```

You should be able to send a *POST* request to *http://localhost:8080/predictions* with a payload like:
```
{
"test_mode": true,
"id_league": 1,
"id_competition": 1,
"id_event": 1,
"id_player": 1
}
```

**IMPORTANT**: add your secret to the header under a variable called **SECRET**

```
//HEADER: SECRET=<my_secret>
```

You should get a response like:

```
{
    "gold": 1,
    "silver": 2,
    "bronze": 3
}
```

**2. Building the container**

```
On the root dir:
> gradle cloudBuild

Building the container for project phoenix-cit and login mylogin
Creating temporary tarball archive of 33 file(s) totalling 100.6 KiB before compression.
Some files were not included in the source upload.

Check the gcloud log [/Users/dviveiros/.config/gcloud/logs/2021.06.24/09.47.41.461561.log] to see which files and the contents of the
default gcloudignore file used (see `$ gcloud topic gcloudignore` to learn
more).

Uploading tarball of [.] to [gs://phoenix-cit_cloudbuild/source/1624553261.804904-ca41336b549d4debb2dd5160aced1b58.tgz]
Created [https://cloudbuild.googleapis.com/v1/projects/phoenix-cit/locations/global/builds/c70f0460-d1d7-4781-aed6-6375b59c9cc7].
Logs are available at [https://console.cloud.google.com/cloud-build/builds/c70f0460-d1d7-4781-aed6-6375b59c9cc7?project=448665479272].
----------------------------- REMOTE BUILD OUTPUT ------------------------------
starting build "c70f0460-d1d7-4781-aed6-6375b59c9cc7"

FETCHSOURCE
Fetching storage object: gs://phoenix-cit_cloudbuild/source/1624553261.804904-ca41336b549d4debb2dd5160aced1b58.tgz#1624553262496564
Copying gs://phoenix-cit_cloudbuild/source/1624553261.804904-ca41336b549d4debb2dd5160aced1b58.tgz#1624553262496564...
/ [1 files][ 16.0 KiB/ 16.0 KiB]                                                
Operation completed over 1 objects/16.0 KiB.                                     
BUILD
Starting Step #0
Step #0: Pulling image: gradle:7.1-jdk8

(...)

046fa1e6609c: Layer already exists
527dfa028f48: Pushed
latest: digest: sha256:91fa4d79bce0993b1df05334138db3a1f1a4d39758a97a3c0517edbabdb3ca54 size: 1166
DONE
--------------------------------------------------------------------------------

ID                                    CREATE_TIME                DURATION  SOURCE                                                                                     IMAGES                                       STATUS
c70f0460-d1d7-4781-aed6-6375b59c9cc7  2021-06-24T16:47:42+00:00  1M6S      gs://phoenix-cit_cloudbuild/source/1624553261.804904-ca41336b549d4debb2dd5160aced1b58.tgz  gcr.io/phoenix-cit/mylogin-player (+1 more)  SUCCESS

BUILD SUCCESSFUL in 1m 11s
1 actionable task: 1 executed
```

**3. Deploying and running the container**

```
> gradle cloudRun

Deploying Cloud Run for project phoenix-cit and login mylogin
Deploying container to Cloud Run service [mylogin-player] in project [phoenix-cit] region [us-west1]
Deploying...
Setting IAM Policy...........done
Creating Revision..............................................................................................................................................................done
Routing traffic..........................................................done
Done.
Service [mylogin-player] revision [mylogin-player-00003-huk] has been deployed and is serving 100 percent of traffic.
Service URL: https://mylogin-player-wuydz5dt4a-uw.a.run.app

BUILD SUCCESSFUL in 23s
1 actionable task: 1 executed
```

Note that you now have the public URL for your player:

```
Service URL: https://mylogin-player-wuydz5dt4a-uw.a.run.app
```

You can test using the same payload described above (port 80). Please inform the admin this URL so we
can configure your player with it.

### Customizing your algorithm

Open the class OlympicsPredictions.java and change the logic for the methods:

```
    /**
     * First round prediction
     * @return Your prediction
     */
    @Override
    public PredictionResponse predict( Integer leagueId, Integer eventId, Boolean isTestMode ) throws
            InvalidEventException {

        //TODO: Implement your predictions for the 1st round here
        PredictionResponse predictionResponse = this.predict( leagueId, eventId, isTestMode, null, null, null );

        logger.debug( "[Olympics 1st round] Returning prediction for league: " + leagueId + ", event: " + eventId
                + ", testMode: " + isTestMode + ". Result = " + predictionResponse );

        return predictionResponse;
    }
```

and

```
    /**
     * Second round prediction. This method will be called by Paul if you win the coin-flip
     * @return Your new prediction
     */
    @Override
    public PredictionResponse predict(Integer leagueId, Integer eventId, Boolean isTestMode,
                                      Map goldMap, Map silverMap, Map bronzeMap) throws InvalidEventException {

        //TODO: Implement your predictions for the 2nd round here
        PredictionResponse predictionResponse = new PredictionResponse();
        Integer gold = -1;
        Integer silver = -1;
        Integer bronze = -1;
        
        // ...
    }
```

That's it :-)

## Questions?

Please use one of our commnunication channels to get in touch with us!

- Admin email: **paul-octopus-admin@ciandt.com**
- General group email: **paul-octopus@ciandt.com**
- Our Google Chat room (probably the easiest way to get your answer)
  
Enjoy!


