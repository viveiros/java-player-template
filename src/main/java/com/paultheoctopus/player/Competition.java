package com.paultheoctopus.player;

/**
 * Constants for competition (Olympics and Paralympics)
 */
public class Competition {

    public static final Integer Olympics = 1;
    public static final Integer Paralympics = 2;
}
