package com.paultheoctopus.player;

public class InvalidEventException extends Exception {

    private Integer eventId;

    public InvalidEventException( Integer eventId ) {
        super( "Invalid event ID: " + eventId);
        this.eventId = eventId;
    }

    public Integer getEventId() {
        return eventId;
    }
}
