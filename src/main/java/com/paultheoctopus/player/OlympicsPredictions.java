package com.paultheoctopus.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class OlympicsPredictions extends Predictions {

    private static final Logger logger = LoggerFactory.getLogger(OlympicsPredictions.class.getName());

    private static final OlympicsPredictions singleton = new OlympicsPredictions();

    public static OlympicsPredictions getInstance() {
        return singleton;
    }

    private OlympicsPredictions() {}

    /**
     * First round prediction
     * @return Your prediction
     */
    @Override
    public PredictionResponse predict( Integer leagueId, Integer eventId, Boolean isTestMode ) throws
            InvalidEventException {

        //TODO: Implement your predictions for the 1st round here
        PredictionResponse predictionResponse = this.predict( leagueId, eventId, isTestMode, null, null, null );

        logger.debug( "[Olympics 1st round] Returning prediction for league: " + leagueId + ", event: " + eventId
                + ", testMode: " + isTestMode + ". Result = " + predictionResponse );

        return predictionResponse;
    }

    /**
     * Second round prediction. This method will be called by Paul if you win the coin-flip
     * @return Your new prediction
     */
    @Override
    public PredictionResponse predict(Integer leagueId, Integer eventId, Boolean isTestMode,
                                      Map goldMap, Map silverMap, Map bronzeMap) throws InvalidEventException {

        //This code is for debug only
        super.logMap(goldMap, "gold");
        super.logMap(silverMap, "silver");
        super.logMap(bronzeMap, "bronze");

        //TODO: Implement your predictions for the 2nd round here
        PredictionResponse predictionResponse = new PredictionResponse();
        Integer gold = -1;
        Integer silver = -1;
        Integer bronze = -1;

        if (eventId == EventOlympics.Cycling_Road_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Taekwondo_58_kg_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Taekwondo_57_kg_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Cycling_Road_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Triathlon_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Diving_Synchronised_10m_Platform_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Artistic_Gymnastics_Team_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Artistic_Gymnastics_Team_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Diving_Synchronised_10m_Platform_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Triathlon_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Surfing_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Surfing_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Artistic_Gymnastics_All_Around_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Artistic_Gymnastics_All_Around_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Table_Tennis_Singles_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Swimming_800m_Freestyle_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Table_Tennis_Singles_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Trampoline_Gymnastics_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Swimming_800m_Freestyle_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Trampoline_Gymnastics_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Tennis_Singles_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Tennis_Singles_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Swimming_4x100m_Medley_Relay_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Swimming_4x100m_Medley_Relay_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Swimming_50m_Freestyle_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Swimming_50m_Freestyle_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_100m_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Artistic_Gymnastics_Floor_Exercise_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_100m_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_Long_Jump_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_Triple_Jump_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Artistic_Gymnastics_Floor_Exercise_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_Long_Jump_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_400m_Hurdles_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_400m_Hurdles_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Skateboarding_Park_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Skateboarding_Park_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_Triple_Jump_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Beach_Volleyball_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Football_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Basketball_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Volleyball_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Handball_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Football_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Beach_Volleyball_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_Marathon_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Basketball_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Volleyball_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Handball_WOMEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else if (eventId == EventOlympics.Athletics_Marathon_MEN) {
            gold = NOC.Afghanistan;
            silver = NOC.Albania;
            bronze = NOC.Algeria;
        } else {
            throw new InvalidEventException( eventId );
        }

        predictionResponse.setGold( gold );
        predictionResponse.setSilver( silver );
        predictionResponse.setBronze( bronze );

        if (goldMap != null) { // 2nd round
            logger.debug("[Olympics 2nd round] Returning prediction for league: " + leagueId + ", event: " + eventId
                    + ", testMode: " + isTestMode + ". Result = " + predictionResponse);
        }

        return predictionResponse;
    }


}
