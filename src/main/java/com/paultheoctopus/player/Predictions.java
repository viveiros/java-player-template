package com.paultheoctopus.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Map;

/**
 * Generic interface for predictions
 */
public abstract class Predictions {

    private static final Logger logger = LoggerFactory.getLogger(Predictions.class.getName());

    public static Predictions getInstance( Integer competitionId ) {
        if (competitionId.equals(Competition.Olympics)) {
            return OlympicsPredictions.getInstance();
        } else if (competitionId.equals(Competition.Paralympics)) {
            return ParalympicsPredictions.getInstance();
        } else {
            throw new RuntimeException("Invalid competition ID: " + competitionId );
        }
    }

    /**
     * First round prediction
     * @return Your prediction
     */
    public abstract PredictionResponse predict( Integer leagueId, Integer eventId, Boolean isTestMode )
            throws InvalidEventException;

    /**
     * Second round prediction. This method will be called by Paul if you win the coin-flip
     * @return Your new prediction
     */
    public abstract PredictionResponse predict( Integer leagueId, Integer eventId, Boolean isTestMode, Map goldMap,
                                       Map silverMap, Map bronzeMap) throws InvalidEventException;

    /**
     * Method used to debug the medal maps sent by Paul the Octopus
     * @param medalMap Medal map
     */
    protected void logMap(Map<String, Double> medalMap, String mapName) {
        if ((medalMap == null) || !logger.isDebugEnabled()) {
            return;
        }
        logger.debug("Listing the content of map named: " + mapName );
        Iterator<String> it = medalMap.keySet().iterator();
        String strNoc;
        while (it.hasNext()) {
            strNoc = it.next();
            logger.debug( "NOC: " + strNoc + ", percentage: " + medalMap.get(strNoc) );
        }
    }
}
