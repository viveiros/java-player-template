package com.paultheoctopus.player;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Payload with the information required to make a prediction. This payload is provided by Paul the Octopus
 * (the game engine) when asking for your predictions for a particular event and competition.
 */
public class PredictionPayload implements Serializable {

    @JsonProperty("test_mode")
    private boolean testMode;
    @JsonProperty("id_league")
    private Integer idLeague;
    @JsonProperty("id_competition")
    private Integer idCompetition;
    @JsonProperty("id_event")
    private Integer idEvent;
    @JsonProperty("id_player")
    private Integer idPlayer;

    @JsonProperty("predictions")
    private PredictionsMaps predictions;

    public boolean isSecondRound() {
        return predictions != null;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public Integer getIdLeague() {
        return idLeague;
    }

    public void setIdLeague(Integer idLeague) {
        this.idLeague = idLeague;
    }

    public Integer getIdCompetition() {
        return idCompetition;
    }

    public void setIdCompetition(Integer idCompetition) {
        this.idCompetition = idCompetition;
    }

    public Integer getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Integer idEvent) {
        this.idEvent = idEvent;
    }

    public Integer getIdPlayer() {
        return idPlayer;
    }

    public void setIdPlayer(Integer idPlayer) {
        this.idPlayer = idPlayer;
    }

    public Map<String, Double> getGold() {
        if (isSecondRound()) {
            return predictions.getGold();
        } else {
            return null;
        }
    }

    public Map<String, Double> getSilver() {
        if (isSecondRound()) {
            return predictions.getSilver();
        } else {
            return null;
        }
    }

    public Map<String, Double> getBronze() {
        if (isSecondRound()) {
            return predictions.getBronze();
        } else {
            return null;
        }
    }
}
