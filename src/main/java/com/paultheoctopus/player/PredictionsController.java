package com.paultheoctopus.player;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class that provides endpoints for getting the predictions for the competition
 */
@RestController
public class PredictionsController {

    //TODO: Put your secret here
    private static final String MY_SECRET = "your secret goes here";
    //TODO: Put your player ID here
    private static final Integer MY_PLAYER_ID = 1; //your player ID goes here

    private static final Logger logger = LoggerFactory.getLogger(PredictionsController.class.getName());
    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * This is the endpoint that Paul the Octopus will use to get your predictions. This is the main method
     * for this implementation
     *
     * @param payload Payload sent by Paul the Octopus
     * @return Your predictions
     */
    @RequestMapping(value = "/predictions", method = RequestMethod.POST,
            produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> onMessageReceived(
            @RequestHeader(value = "SECRET", required = false) String secret,
            @RequestBody String payload) {
        logger.info("Message received from Paul the Octopus. Payload: " + payload);

        //Checks if the secret is in the header
        if ((secret == null) || (secret.trim().length() == 0)) {
            logger.warn( "No secret was found on Paul's request... returning FORBIDDEN");
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("No secret was found on Paul's request... returning FORBIDDEN");
        }
        //Checks if the secret is valid (please, change the MY_SECRET constant to reflect yours)
        if (!secret.equals(MY_SECRET)) {
            logger.warn( "The secret in the header is not valid. Unauthorized call.");
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("The secret in the header is not valid. Unauthorized call.");
        }

        //parses the payload
        PredictionPayload predictionPayload;
        try {
            predictionPayload = mapper.readValue( payload, PredictionPayload.class );
        } catch (JsonProcessingException e) {
            logger.error("Error parsing prediction payload", e);
            return ResponseEntity.internalServerError().build();
        }
        logger.debug( "Test mode: " + predictionPayload.isTestMode() );
        logger.debug( "ID League: " + predictionPayload.getIdLeague() );
        logger.debug( "ID Competition: " + predictionPayload.getIdCompetition() );
        logger.debug( "ID Event: " + predictionPayload.getIdEvent() );
        logger.debug( "ID Player: " + predictionPayload.getIdPlayer() );

        //checks the player ID
        if (!MY_PLAYER_ID.equals(predictionPayload.getIdPlayer())) {
            logger.warn( "Invalid player ID. Expected = " + MY_PLAYER_ID + ", Actual = "
                    + predictionPayload.getIdPlayer());
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Invalid player ID. Expected = " + MY_PLAYER_ID + ", Actual = "
                            + predictionPayload.getIdPlayer());
        }

        //Creates your prediction
        Predictions predictions = Predictions.getInstance( predictionPayload.getIdCompetition() );
        PredictionResponse predictionResponse;
        try {
            if (!predictionPayload.isSecondRound()) { // 1st round
                predictionResponse = predictions.predict(predictionPayload.getIdLeague(),
                        predictionPayload.getIdEvent(), predictionPayload.isTestMode());
            } else { // 2nd round
                predictionResponse = predictions.predict(predictionPayload.getIdLeague(),
                        predictionPayload.getIdEvent(), predictionPayload.isTestMode(),
                        predictionPayload.getGold(), predictionPayload.getSilver(), predictionPayload.getBronze());
            }
        } catch ( InvalidEventException e ) {
            logger.error( "Error getting the prediction for event " + predictionPayload.getIdEvent(), e );
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }

        //Serializes the prediction into the expected JSON format
        String predictionJson;
        try {
            predictionJson = mapper.writeValueAsString(predictionResponse);
        } catch (JsonProcessingException e) {
            logger.error("Error generating the prediction for object: " + predictionResponse, e);
            return ResponseEntity.internalServerError().build();
        }

        //Successful prediction :)
        return ResponseEntity.ok(predictionJson);
    }
}
