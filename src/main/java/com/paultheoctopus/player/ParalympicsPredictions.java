package com.paultheoctopus.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Predictions for the Paralympics game
 */
public class ParalympicsPredictions extends Predictions {

    private static final Logger logger = LoggerFactory.getLogger(ParalympicsPredictions.class.getName());

    private static final ParalympicsPredictions singleton = new ParalympicsPredictions();

    public static ParalympicsPredictions getInstance() {
        return singleton;
    }

    private ParalympicsPredictions() {}

    /**
     * First round prediction
     * @return Your prediction
     */
    @Override
    public PredictionResponse predict( Integer leagueId, Integer eventId, Boolean isTestMode ) {
        //TODO: Implement your predictions for the 1st round here
        PredictionResponse predictionResponse = new PredictionResponse();
        predictionResponse.setGold( -2 );
        predictionResponse.setSilver( -2 );
        predictionResponse.setBronze( -2 );

        logger.debug( "[Paralympics 1st round] Returning prediction for league: " + leagueId + ", event: " + eventId
                + ", testMode: " + isTestMode + ". Result = " + predictionResponse );

        return predictionResponse;
    }

    /**
     * Second round prediction. This method will be called by Paul if you win the coin-flip
     * @return Your new prediction
     */
    @Override
    public PredictionResponse predict(Integer leagueId, Integer eventId, Boolean isTestMode,
                                      Map goldMap, Map silverMap, Map bronzeMap) {
        //This code is for debug only
        super.logMap(goldMap, "gold");
        super.logMap(silverMap, "silver");
        super.logMap(bronzeMap, "bronze");

        //TODO: Implement your predictions for the 2nd round here
        PredictionResponse predictionResponse = new PredictionResponse();
        predictionResponse.setGold( -2 );
        predictionResponse.setSilver( -2 );
        predictionResponse.setBronze( -2 );

        logger.debug( "[Paralympics 2nd round] Returning prediction for league: " + leagueId + ", event: " + eventId
                + ", testMode: " + isTestMode + ". Result = " + predictionResponse );

        return predictionResponse;
    }
}
