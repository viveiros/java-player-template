package com.paultheoctopus.player;

/**
 * Constants for the Olympics events
 */
public final class EventOlympics {

    public static final Integer Cycling_Road_MEN = 1;
    public static final Integer Taekwondo_58_kg_MEN = 2;
    public static final Integer Taekwondo_57_kg_WOMEN = 3;
    public static final Integer Cycling_Road_WOMEN = 4;
    public static final Integer Triathlon_MEN = 5;
    public static final Integer Diving_Synchronised_10m_Platform_MEN = 6;
    public static final Integer Artistic_Gymnastics_Team_MEN = 7;
    public static final Integer Artistic_Gymnastics_Team_WOMEN = 8;
    public static final Integer Diving_Synchronised_10m_Platform_WOMEN = 9;
    public static final Integer Triathlon_WOMEN = 10;
    public static final Integer Surfing_MEN = 11;
    public static final Integer Surfing_WOMEN = 12;
    public static final Integer Artistic_Gymnastics_All_Around_MEN = 13;
    public static final Integer Artistic_Gymnastics_All_Around_WOMEN = 14;
    public static final Integer Table_Tennis_Singles_WOMEN = 15;
    public static final Integer Swimming_800m_Freestyle_MEN = 16;
    public static final Integer Table_Tennis_Singles_MEN = 17;
    public static final Integer Trampoline_Gymnastics_WOMEN = 18;
    public static final Integer Swimming_800m_Freestyle_WOMEN = 19;
    public static final Integer Trampoline_Gymnastics_MEN = 20;
    public static final Integer Tennis_Singles_WOMEN = 21;
    public static final Integer Tennis_Singles_MEN = 22;
    public static final Integer Swimming_4x100m_Medley_Relay_MEN = 23;
    public static final Integer Swimming_4x100m_Medley_Relay_WOMEN = 24;
    public static final Integer Swimming_50m_Freestyle_MEN = 25;
    public static final Integer Swimming_50m_Freestyle_WOMEN = 26;
    public static final Integer Athletics_100m_WOMEN = 27;
    public static final Integer Artistic_Gymnastics_Floor_Exercise_MEN = 28;
    public static final Integer Athletics_100m_MEN = 29;
    public static final Integer Athletics_Long_Jump_MEN = 30;
    public static final Integer Athletics_Triple_Jump_WOMEN = 31;
    public static final Integer Artistic_Gymnastics_Floor_Exercise_WOMEN = 32;
    public static final Integer Athletics_Long_Jump_WOMEN = 33;
    public static final Integer Athletics_400m_Hurdles_MEN = 34;
    public static final Integer Athletics_400m_Hurdles_WOMEN = 35;
    public static final Integer Skateboarding_Park_WOMEN = 36;
    public static final Integer Skateboarding_Park_MEN = 37;
    public static final Integer Athletics_Triple_Jump_MEN = 38;
    public static final Integer Beach_Volleyball_WOMEN = 39;
    public static final Integer Football_WOMEN = 40;
    public static final Integer Basketball_MEN = 41;
    public static final Integer Volleyball_MEN = 42;
    public static final Integer Handball_MEN = 43;
    public static final Integer Football_MEN = 44;
    public static final Integer Beach_Volleyball_MEN = 45;
    public static final Integer Athletics_Marathon_WOMEN = 46;
    public static final Integer Basketball_WOMEN = 47;
    public static final Integer Volleyball_WOMEN = 48;
    public static final Integer Handball_WOMEN = 49;
    public static final Integer Athletics_Marathon_MEN = 50;
}
