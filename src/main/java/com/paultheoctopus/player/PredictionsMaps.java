package com.paultheoctopus.player;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * Data object to handle the predictions map for the second round (twist).
 */
public class PredictionsMaps implements Serializable {

    @JsonProperty("gold")
    private Map<String,Double> gold;
    @JsonProperty("silver")
    private Map<String,Double> silver;
    @JsonProperty("bronze")
    private Map<String,Double> bronze;

    public Map<String, Double> getGold() {
        return gold;
    }

    public void setGold(Map<String, Double> gold) {
        this.gold = gold;
    }

    public Map<String, Double> getSilver() {
        return silver;
    }

    public void setSilver(Map<String, Double> silver) {
        this.silver = silver;
    }

    public Map<String, Double> getBronze() {
        return bronze;
    }

    public void setBronze(Map<String, Double> bronze) {
        this.bronze = bronze;
    }
}
