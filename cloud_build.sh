#!/usr/bin/env bash
echo Building the container for project $1 and login $2
gcloud builds submit --config cloudbuild.yaml . --substitutions=_GCP_PROJECT=$1,_MY_LOGIN=$2